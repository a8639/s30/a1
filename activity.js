const HTTP = require('http');
const express = require('express');
const mongoose = require('mongoose');
//express() is the server stored in the variable app
const app = express()
const PORT = 3000;

//dotenv to hide sensitive info
const dotenv = require('dotenv');
dotenv.config();

//mongoose connection
mongoose.connect(
    process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});


//DB connection notification
//use connection property of mongoose
const db = mongoose.connection;
db.on("error", console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to database`));

app.use(express.json())
app.use(express.urlencoded({extended:true}))



const userSchema = new mongoose.Schema(
    {
        username: String,
        password: String
    }
)


const User = mongoose.model(`User`, userSchema);





app.post("/signup", (req, res) => {
    const result = User.findOne({username: req.body.username}).then((result, err) => {
        console.log(result)
        if(result != null && result.username == req.body.username){
            return res.send(`Duplicate user found`)
        } else {
            let newUser = new User ({
                username: req.body.username
            })
            newUser.save().then((result, err) => {
                if(result){
                    return res.send(`New user registered`)
                } else {
                    return res.send(err)
                }
            })

        }
    })
})





app.listen(PORT, () => console.log(`Server connected at port ${PORT}`))